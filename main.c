#include "led.h"
#include "keyboard.h"
 
void Delay(int iTimeInMs){
	int iCycle;
	int iNumberOfCycles = 12000 * iTimeInMs;
	
	for (iCycle = 0; iCycle < iNumberOfCycles; iCycle++) {}
}


int main(){ //git test

	LedInit();
	ButtonInit();
	
	while(1){ 
		Delay(100);
		switch(eKeyboardRead()){
			case BUTTON_1:
				LedStepRight();
				break;			
			case BUTTON_2:
				LedStepLeft();
				break;
			case BUTTON_0:
				break;
			case BUTTON_3:
				break;
			case RELASED:
				break;
			default:{}
		}
	}
}
